import React, {lazy, Suspense} from "react";
import { View, Text } from "react-native";

import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";

const Camera = lazy(() => import('./pages/Camera'));

const Stack = createNativeStackNavigator();

export default function App() {
  return (
    <Suspense fallback={<Text>Loading...</Text>}> 
      <NavigationContainer>
        <Stack.Navigator
          initialRouteName="Camera"
          screenOptions={{headerShown: false}}
        >
          <Stack.Screen name="Camera" component={Camera}/>
        </Stack.Navigator>
      </NavigationContainer>
    </Suspense>
  );
}

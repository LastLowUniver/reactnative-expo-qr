import React, { useEffect, useState, useRef } from "react";
import { View, Text, SafeAreaView,ScrollView, TouchableOpacity, StyleSheet, Animated, FlatList } from "react-native";

import tw from 'tailwind-react-native-classnames'; 
import { BarCodeScanner } from "expo-barcode-scanner";

import Icon from 'react-native-vector-icons/FontAwesome';

export default function Camera() {

    const [hasPermission, setHasPermission] = useState(null),
        [scanned, setScanned] = useState(false),
        [type, setType] = useState(BarCodeScanner.Constants.Type.back),
        [X, setX] = useState(0),
        [Y, setY] = useState(0),
        [width, setWidth] = useState(0),
        [height, setHeight] = useState(0),
        [bottom, setBottom] = useState(-1000),
        [dataMenu, setDataMenu] = useState([]),
        [textPopup, setTextPopup] = useState('')

    const fadeAnim = useRef(new Animated.Value(1000)).current
    const popupMenuAnim = useRef(new Animated.Value(1000)).current

    useEffect(() => {
        (async () => {
            const {status} = await BarCodeScanner.requestPermissionsAsync();
            setHasPermission(status === 'granted');
        })();
    }, []);

    const getInfo = (data) => {
        
            return fetch(data)
                .then((response) => response.json())
                .then((json) => {
                    setDataMenu(json)
                })
                .catch((error) => {
                    console.error(error);
                });
        
    }

    const ValidURL = (value) => {
        return /^(https?|ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(value);
    }

    const menuUp = (fadeAnim) => {
        Animated.timing(
          fadeAnim,
          {
            toValue: 100,
            duration: 300,
            useNativeDriver: true
          }
        ).start();
    }

    const menuDown = (fadeAnim) => {
        Animated.timing(
          fadeAnim,
          {
            toValue: 1000,
            duration: 300,
            useNativeDriver: true
          }
        ).start();
    }


    const handleSuccuss = ({bounds, data}) => {
        setScanned(true)
        if (ValidURL(data)) {
            getInfo(data.toString())
            menuUp(fadeAnim)
        }else {
            setTextPopup(data)
            menuUp(popupMenuAnim)
        }
    }

    const elem = () => {
        setScanned(false)
    }

    if(hasPermission === null || hasPermission === false) {
        return <Text>No access</Text>
    }

    const styles = StyleSheet.create({
        view: {
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
        },
        cameraContainer: {
            marginHorizontal: 0, marginLeft: 0, marginStart: 0,
            paddingHorizontal: 0, paddingLeft: 0, paddingStart: 0,
            position: 'relative',
            height: '100%',
            width: '130%',
            padding: 0,
        },
        infoPopupBlock: {
            flex: 1,
            height: 'auto',
            width: '100%',
            position: 'absolute',
            translateY: popupMenuAnim,
            zIndex: 2000,
            backgroundColor: '#fff',
            borderRadius: 20
        },
        infoWindowBlock: {
            flex: 1,
            height: '90%',
            width: '100%',
            position: 'absolute',
            translateY: fadeAnim,
            zIndex: 2000,
            backgroundColor: '#fff',
            borderTopLeftRadius: 20,
            borderTopRightRadius: 20,
        },
        infoOpacityBlock: {
            width: '90%',
            backgroundColor: '#d0f4ff',
            margin: 10,
            padding: 10,
            borderRadius: 5
        }
    });

    return (
            <View style={tw`flex-1`}>
                <View style={styles.view}> 
                    <BarCodeScanner
                        BarCodeScannerSettings = {{
                            barCodeTypes: [BarCodeScanner.Constants.BarCodeType.qr],
                        }}
                        onBarCodeScanned={scanned ? undefined : handleSuccuss}
                        style={styles.cameraContainer}
                    >
                        <View style={tw`flex-1 flex-row bg-transparent`}>
                            <TouchableOpacity 
                            disabled={!scanned}
                            style={scanned ? tw`bg-blue-400 flex-auto self-end p-6` : tw`bg-gray-500 flex-auto self-end p-6`}
                            onPress= {() => {elem()}}
                            >
                                <Text style={tw`text-white text-center`}>
                                    Scanning again
                                </Text>
                            </TouchableOpacity>
                        </View>
                    </BarCodeScanner>
                </View>
                <Animated.View
                style={styles.infoPopupBlock}>
                    <Icon style={{position: 'absolute',right: 20,top: -50}} name="close" size={40} color="#a7a7a7" onPress={() => {menuDown(popupMenuAnim)}}/>
                    <Text style={tw`text-center p-5`}>{textPopup}</Text>
                </Animated.View>
                <Animated.View
                style={styles.infoWindowBlock}>
                    <Icon style={{position: 'absolute',right: 20,top: -50}} name="close" size={40} color="#a7a7a7" onPress={() => {menuDown(fadeAnim); setDataMenu([])}}/>
                    <FlatList
                        style={{width: '100%', height: '100%'}}
                        contentContainerStyle={{
                            alignItems: 'center',
                            justifyContent: 'space-around'
                        }}
                        data={dataMenu}
                        renderItem={({item}) => <TouchableOpacity style={styles.infoOpacityBlock}><Text>{item.title}</Text></TouchableOpacity>}
                        keyExtractor={item => item.id.toString()}
                    >
                        
                    </FlatList>
                </Animated.View>
            </View>
    )
}
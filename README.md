# A project on React Native for the university, an introduction to the direction of computers.

Applications for reading QR codes and displaying data in their presence

[Link to the QR code for an example when using the application/1](http://qrcoder.ru/code/?https%3A%2F%2Fjsonplaceholder.typicode.com%2Ftodos&4&0)

[Link to the QR code for an example when using the application/2](http://qrcoder.ru/code/?https%3A%2F%2Fjsonplaceholder.typicode.com%2Fposts&4&0)

The application is a test, so it is recommended to use only the QR codes given above, or those that do not have a link, but ordinary text
